### A Pluto.jl notebook ###
# v0.19.3

using Markdown
using InteractiveUtils

# ╔═╡ f65ab9ec-cc8e-11ec-3623-b9bd98bc4aa8
begin
	using Pkg;
	Pkg.add("LinearAlgebra");
	Pkg.add("Plots");
	Pkg.add("PyPlot");
	Pkg.add("JuMP")
	Pkg.add("GLPK")
end

# ╔═╡ ca5420ae-cd0d-493f-8b0f-30790a8c0225
begin
	using JuMP
	using GLPK
	using LinearAlgebra
	using Plots
	pyplot()
end

# ╔═╡ effda066-a643-4e65-842f-52ca92cd5794
begin
	x_v = LinRange(-2,15,100)
	plot([x_v], [x_v .+ 7.5], label ="Y=x + 7.5")
	plot!([x_v], [-2x_v .+ 20], label ="Y= -2x + 20")
end

# ╔═╡ ae6293fb-31df-40f7-8aab-5f6ccef20978
begin
	y_v = LinRange(-2,15,100)
	plot([0*x_v], [y_v],label ="Y Axis")
	plot!([x_v], [0*x_v],label ="X Axis")
	plot!([x_v], [0*x_v .+ 7.5], label ="Y=7.5")
	plot!([0*x_v .+ 10], [y_v],label ="X=10")
	plot!(title = "Constraints")
end

# ╔═╡ 0e37e321-4937-4cb2-aed2-b0a32ee7c5ca
begin
	prgrm = Model()
	set_optimizer(prgrm, GLPK.Optimizer)
end

# ╔═╡ 09f7830c-deab-4434-8488-c1f64db771cd
begin
	@variable(prgrm, 0<=x)
	@variable(prgrm, 0<=y)
end

# ╔═╡ f1452521-0fdb-46e8-9a6f-2cd9b8159f3a
begin
	@constraint(prgrm, x <= 10)
	@constraint(prgrm, y <= 7.5)
end

# ╔═╡ d3eb6bd6-1d00-4289-a0b2-4c3734e6d1c7
@objective(prgrm, Max, x+2y)

# ╔═╡ aa64964b-6d2d-45a0-8564-954b5581ccc0
optimize!(prgrm)

# ╔═╡ a9cdf8e6-c608-43f0-95d9-d5f9d11b846e
begin
	value.(x)
	value.(y)
end

# ╔═╡ 1a4be27d-0144-411f-ba25-916630bb7c5d
plot!([value.(x)], [value.(y)], seriestype = :scatter, label="Optimum")

# ╔═╡ c18b8367-f453-4665-a0b0-609fd58c6f12


# ╔═╡ Cell order:
# ╠═f65ab9ec-cc8e-11ec-3623-b9bd98bc4aa8
# ╠═ca5420ae-cd0d-493f-8b0f-30790a8c0225
# ╠═effda066-a643-4e65-842f-52ca92cd5794
# ╠═ae6293fb-31df-40f7-8aab-5f6ccef20978
# ╠═0e37e321-4937-4cb2-aed2-b0a32ee7c5ca
# ╠═09f7830c-deab-4434-8488-c1f64db771cd
# ╠═f1452521-0fdb-46e8-9a6f-2cd9b8159f3a
# ╠═d3eb6bd6-1d00-4289-a0b2-4c3734e6d1c7
# ╠═aa64964b-6d2d-45a0-8564-954b5581ccc0
# ╠═a9cdf8e6-c608-43f0-95d9-d5f9d11b846e
# ╠═1a4be27d-0144-411f-ba25-916630bb7c5d
# ╠═c18b8367-f453-4665-a0b0-609fd58c6f12
