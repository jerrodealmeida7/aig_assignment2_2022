### A Pluto.jl notebook ###
# v0.19.3

using Markdown
using InteractiveUtils

# ╔═╡ ea090cd0-ca0d-11ec-1ee5-11e5735c0a46
using Pkg

# ╔═╡ 83cae2ab-18a7-42f3-948c-8e3753ee5571
Pkg.activate("Project.toml")

# ╔═╡ e2e25def-0a2e-429b-b4e0-2eb56eb148de
using DataStructures

# ╔═╡ d7b4295c-ffb7-4db3-a9a9-99ee5add064c
using Markdown

# ╔═╡ 73266bd0-524c-4002-b0df-b93f5ed949c1
mutable struct Office
		itemCount::Int64 
		Position::Tuple{Int64, Int64}
	end

# ╔═╡ 1b47e089-02d8-42b3-89b4-b47ac533f2b0
struct Action
		Direction::String
		Move::Tuple{Int64, Int64}
		Cost::Int64
	end

# ╔═╡ eb950129-1e8c-40bd-8c78-39982db7a94b
mutable struct AgentState
		Building::Matrix{Int64}
		StateCost::Int64
		collectedParcels::Int64
		CurrentOffice::Office
		path::Vector{String}
	end

# ╔═╡ de8549e0-aca2-4e56-b926-b5df97242987
begin
	global Mu = Action("Up",(-1,0)		, 1)
	global Md = Action("Down",(1,0)		, 1)
	global Mw = Action("West",(0,-1)	, 2)
	global Me = Action("East",(0,1)		, 2) 
	global Co = Action("Collect",(0,0)  , 5)
end

# ╔═╡ c783b7c7-2866-4b5a-96c4-a4159d9e99b0
begin
	global start = Office(0, (1,1))
	global second = Office(0, (1,1))
	global third = Office(0, (1,1))
	global fourth = Office(0, (1,1))
end

# ╔═╡ 50995d31-2c09-40f8-aada-40c93516eb1e
global ParcelsToBeCollected =  4

# ╔═╡ fbfdea23-5a91-41c4-81aa-d0c0a3ddbf13
global CompanyX_Building = 
	   [0 0 0;
        1 0 0; 
        0 1 0;
        1 1 0;
        0 0 0] 

# ╔═╡ 388cc22d-5eb4-42e0-97a8-7091b4afb458
global intialState = AgentState(CompanyX_Building, 0, 0, start,  [])

# ╔═╡ fd9d7884-f9c2-497e-8f17-8ba680e94e99
begin
	global moves = 0
	global goal =  false
	global CanCollect = true 
	global checkVal = false 
	global validState = true
end

# ╔═╡ 185552f6-35d0-4c1c-a070-a9a81689a172
begin
	global PrevStatePosition = start.Position 
	global VisitedNodes = []
	push!(VisitedNodes, intialState)
	global CurrentState = () 
	global Path = []; 
end

# ╔═╡ 779a3704-0904-4a20-a552-390a8bbb0d28
function getTotalCost(m, cost, n ,nextMoveCost)
	
	gn = cost + nextMoveCost
	 if m[n[1],n[2]] >= 1 && CanCollect
		hn = (ParcelsToBeCollected - 1 ) * 3
	else
		hn = ParcelsToBeCollected * 3
	end
	fn = gn + hn
	return fn
end

# ╔═╡ 31a7862b-9651-43ec-99e6-60bac73c36b1
 function stateIsValid(PrevState, CurrentState)
	 
	 if PrevState.CurrentOffice.Position != start
		 if PrevState.CurrentOffice.Position == CurrentState.CurrentOffice.Position
			 if PrevState.collectedParcels == CurrentState.collectedParcels 
				 validState = false
			 end
		 end
	 end
	 validState = true
 end

# ╔═╡ eaadcc79-b0ae-4617-9a07-a526f0cce8f8
function getAvailableMovesList(TempState)

Available_Moves = Action[]
	
	tempP_Mu = 
		TempState.CurrentOffice.Position[1] + Mu.Move[1] , 
		TempState.CurrentOffice.Position[2] + Mu.Move[2] 

	tempP_Md = 
		TempState.CurrentOffice.Position[1] + Md.Move[1] , 
		TempState.CurrentOffice.Position[2] + Md.Move[2] 
	
	tempP_Me = 
		TempState.CurrentOffice.Position[1] + Me.Move[1] , 
		TempState.CurrentOffice.Position[2] + Me.Move[2] 
	
	tempP_Mw = 
		TempState.CurrentOffice.Position[1] + Mw.Move[1] , 
		TempState.CurrentOffice.Position[2] + Mw.Move[2] 

	n = TempState.CurrentOffice.Position
	
	if TempState.Building[n[1],n[2]] >= 1 && CanCollect
	push!(Available_Moves, Co)
	end

	if 1 <= tempP_Mw[1] && 1 <= tempP_Mw[2]
		push!(Available_Moves, Mw)
	end
	
	if 1 <= tempP_Md[1] && 1 <= tempP_Md[2]
		push!(Available_Moves, Md)
	end
	
	if 1 <= tempP_Mu[1] && 1 <= tempP_Mu[2]
		push!(Available_Moves, Mu)
	end
 
	if 1 <= tempP_Me[1] && 1 <= tempP_Me[2]
		push!(Available_Moves, Me)
	end

	return Available_Moves
end

# ╔═╡ 4b284e2b-f5c3-4f95-83e9-230f1152059f
function buildNewState(CompanyX_Building,hurisiticVal,parcelsCollected,Coffice,Cpath, nextM, action)
	
	if action == Co
		x = CompanyX_Building[Coffice.Position[1], Coffice.Position[2]]
		CompanyX_Building[Coffice.Position[1], Coffice.Position[2]] = x - 1
		canCollect = false
	end
		
	a = []

	for i in Cpath
		push!(a, i)
	end

		push!(a, nextM)
	
	newState = AgentState(CompanyX_Building,hurisiticVal,parcelsCollected,Coffice,a)
	
	return newState
	end

# ╔═╡ a9bbacc7-b20c-4aaf-a9c6-a994b62c5bfc
function startSearch(state)	
	stateList = []
	parcels = 0

	AvailableMoves = getAvailableMovesList(state) 

	for i in AvailableMoves 
		TempState = state;
		if i == Co

				println("Collecting...")
				
			 MX = TempState.Building
			 P_C = TempState.collectedParcels + 1
			 P =	
				(TempState.CurrentOffice.Position[1] + i.Move[1] , TempState.CurrentOffice.Position[2] + i.Move[2])
			 C_O = Office(TempState.Building[P[1],P[2]], P)
			 H_V = getTotalCost(TempState.Building,TempState.StateCost, P, i.Cost)
			 O_P = TempState.path
			 C_P =  i.Direction
							
			if stateIsValid(state, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
				push!(stateList, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
			end
			CanCollect = false;
		else
			MX = TempState.Building
			 P_C = TempState.collectedParcels
			 P =	
				(TempState.CurrentOffice.Position[1] + i.Move[1] , TempState.CurrentOffice.Position[2] + i.Move[2])
			 C_O = Office(TempState.Building[P[1],P[2]], P)
			 H_V = getTotalCost(TempState.Building,TempState.StateCost, P, i.Cost)
			 O_P = TempState.path
			 C_P =  i.Direction
		CanCollect = true;
			if stateIsValid(state, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
				push!(stateList, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
			end
		end
	end
	
	sort!(stateList, by = x -> x.StateCost) 
	sort!(stateList, by = x -> x.collectedParcels, rev=true) 
	return stateList
end

# ╔═╡ b907b697-a194-4a2d-aff1-725942363f68
function run(maze, state)
	TempState = state
	nodeList = []
		push!(nodeList, state)
	while nodeList[1].collectedParcels != ParcelsToBeCollected
	TempNodeList = startSearch(nodeList[1]) 
	empty!(nodeList)
	for i in TempNodeList
		push!(nodeList, i)
	end
	end
	return nodeList
end

# ╔═╡ 7449f1ca-25aa-47e0-b3c5-d98dba9f5a30
begin
	res = run(CompanyX_Building, intialState)
end

# ╔═╡ Cell order:
# ╠═ea090cd0-ca0d-11ec-1ee5-11e5735c0a46
# ╠═83cae2ab-18a7-42f3-948c-8e3753ee5571
# ╠═e2e25def-0a2e-429b-b4e0-2eb56eb148de
# ╠═d7b4295c-ffb7-4db3-a9a9-99ee5add064c
# ╠═73266bd0-524c-4002-b0df-b93f5ed949c1
# ╠═1b47e089-02d8-42b3-89b4-b47ac533f2b0
# ╠═eb950129-1e8c-40bd-8c78-39982db7a94b
# ╠═de8549e0-aca2-4e56-b926-b5df97242987
# ╠═c783b7c7-2866-4b5a-96c4-a4159d9e99b0
# ╠═50995d31-2c09-40f8-aada-40c93516eb1e
# ╠═fbfdea23-5a91-41c4-81aa-d0c0a3ddbf13
# ╠═388cc22d-5eb4-42e0-97a8-7091b4afb458
# ╠═fd9d7884-f9c2-497e-8f17-8ba680e94e99
# ╠═185552f6-35d0-4c1c-a070-a9a81689a172
# ╠═779a3704-0904-4a20-a552-390a8bbb0d28
# ╠═31a7862b-9651-43ec-99e6-60bac73c36b1
# ╠═eaadcc79-b0ae-4617-9a07-a526f0cce8f8
# ╠═4b284e2b-f5c3-4f95-83e9-230f1152059f
# ╠═a9bbacc7-b20c-4aaf-a9c6-a994b62c5bfc
# ╠═b907b697-a194-4a2d-aff1-725942363f68
# ╠═7449f1ca-25aa-47e0-b3c5-d98dba9f5a30
